import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class MenuResultadosController {
	
	@FXML
	private BarChart<String, Integer> resultadosGrafico;
	
	@FXML
	private Label bestLabel;
	
	@FXML
	private TableColumn<Pedido, String> inicioColumn, finColumn;
	
	@FXML
	private TableView<Pedido> pedidosTable;
	
	XYChart.Series<String, Integer> serieResultadosGrafico = new XYChart.Series<>();
	Mapa mapa = Mapa.getInstance();
	
	@FXML
	public void initialize() {
		rellenarDatos();
		inicioColumn.setCellValueFactory(new PropertyValueFactory<>("inicioS"));
		finColumn.setCellValueFactory(new PropertyValueFactory<>("finS"));
		pedidosTable.setItems(MenuPedidosController.contenidoTabla);
		resultadosGrafico.getData().add(serieResultadosGrafico);
	}
	
	private void rellenarDatos() {
		for(String key : mapa.puntuaciones.keySet()) {
			serieResultadosGrafico.getData().add(new XYChart.Data<>(key, mapa.puntuaciones.get(key)));
		}
		
		mapa.encontrarSolucion();
		bestLabel.setText(mapa.solucion + " u");
	}
	
	@FXML
	private void reiniciarReparto() throws IOException {
		Mapa.resetInstance();
		
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("MenuTemplate.fxml"));
		Parent root = (Parent) fxmlLoader.load();
		Scene scene = new Scene(root, 600, 400);
		Main.window.setScene(scene);
	}

}
