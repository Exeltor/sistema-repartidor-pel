

import java.io.IOException;
import java.util.ArrayList;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;

public class MenuTemplateController {
	
	@FXML
	private AnchorPane regionGrid;
	@FXML
	private Label errorLabel, xLabel, yLabel;
	@FXML
	private Button calcularButton;
	
	ArrayList<Circle> puntos = new ArrayList<>();
	ArrayList<Line> conexiones = new ArrayList<>();
	ArrayList<String> letras = new ArrayList<>();
	
	@FXML
	public void initialize() {
		rellenarLetras();
		regionGrid.setOnMouseClicked(e -> {
			errorLabel.setText("");
			addNode(e.getX(), e.getY());
		});
		
		regionGrid.setOnMouseMoved(e -> {
			errorLabel.setText("X: " + e.getX() + ", Y: " + e.getY()) ;
		});
	}
	
	private void addNode(double x, double y) {
		Circle nodo = new Circle();
		Label letra = new Label(letras.get(puntos.size()));
		nodo.setRadius(10);
		nodo.setFill(Color.WHITE);
		nodo.setCenterX(x);
		nodo.setCenterY(y);
		letra.setLayoutX(x-4);
		letra.setLayoutY(y-7);
		connectNodes(nodo);
		regionGrid.getChildren().addAll(nodo, letra);
		puntos.add(nodo);
		xLabel.setText("X: " + String.valueOf(x));
		yLabel.setText("Y: " + String.valueOf(y));
	}
	
	private void connectNodes(Circle nodo) {
		for(int i = 0; i < puntos.size(); i++) {
			if(puntos.get(i) == nodo) {
				continue;
			}
			
			Line conexion = new Line();
			conexion.setStroke(Color.WHITE);
			conexion.setStartX(nodo.getCenterX());
			conexion.setStartY(nodo.getCenterY());
			conexion.setEndX(puntos.get(i).getCenterX());
			conexion.setEndY(puntos.get(i).getCenterY());
			
			regionGrid.getChildren().add(conexion);
			conexiones.add(conexion);
		}
	}
	
	private void rellenarLetras() {
		for(int i = 65; i <= 90; i++) {
			letras.add(String.valueOf((char) i));
		}
	}
	
	@FXML
	private void calcularCamino() throws IOException {
		if(puntos.size() < 3) {
			errorLabel.setText("A�ade mas puntos");
		} else {
			Mapa mapa = Mapa.getInstance();
			
			for(int i = 0; i < puntos.size(); i++) {
				String letra = letras.get(i);
				int x = (int)puntos.get(i).getCenterX();
				int y = (int)puntos.get(i).getCenterY();
				
				mapa.addNode(letra, x, y);
			}
			
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("MenuPedidosTemplate.fxml"));
			Parent root = (Parent) fxmlLoader.load();
			Scene scene = new Scene(root, 600, 400);
			Main.window.setScene(scene);
		}
	}
}
