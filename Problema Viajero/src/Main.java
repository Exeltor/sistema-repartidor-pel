
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;

public class Main extends Application {
	public static Stage window = new Stage();
	
	@Override
	public void start(Stage primaryStage) {
		try {
			window = primaryStage;
			AnchorPane root = (AnchorPane)FXMLLoader.load(getClass().getResource("/MenuTemplate.fxml"));
			Scene scene = new Scene(root,600,400);
			//scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			window.setScene(scene);
			window.setResizable(false);
			window.setOnCloseRequest(new EventHandler<WindowEvent>() {
				public void handle(WindowEvent we) {
					System.exit(0);
				}
			});
			window.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
	
	
}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
//	public static void main(String[] args) {
//		
//		
//		
//		Mapa mapa = new Mapa();
//		
//		mapa.addNode("A", 0,0);
//		mapa.addNode("B",0,1);
//		mapa.addNode("C", 0, 7);
//		mapa.addNode("D", 10, 10);
//		mapa.addNode("E", 20, 20);
//		mapa.addNode("F", 40, 20);
//		
//		mapa.generarPedido("B", "C");
//		mapa.generarPedido("F", "D");
//		mapa.generarPedido("F", "D");
//		
//		mapa.encontrarCamino("A");
//		
//		System.out.println(mapa);
//	}


