
public class Conexion {
	
	private Nodo start, end;
	private int cost;
	
	public Conexion(Nodo start, Nodo end) {
		this.start = start;
		this.end = end;
		
		int startX = start.x;
		int startY = start.y;
		int finishX = end.x;
		int finishY = end.y;
		
		if(startX == finishX) {
			cost = Math.abs(startY - finishY);
		} else if (startY == finishY) {
			cost = Math.abs(startX - finishX);
		} else if(startX != finishX && startY != finishY){
			int xDiff = Math.abs(startX - finishX);
			int yDiff = Math.abs(startY - finishY);
			cost = (int) Math.sqrt(Math.pow(xDiff, 2) + Math.pow(yDiff, 2));
		} else if(startX == finishX && startY == finishY) {
			cost = 0;
		}
	}
	
	public boolean checkStartNodeLabel(String label) {
		if(this.start.getLabel().equals(label) || this.end.getLabel().equals(label)) {
			return true;
		}
		
		return false;
	}
	
	public boolean checkEndNodeLabel(String label) {
		if(this.end.getLabel().equals(label) || this.start.getLabel().equals(label)) {
			return true;
		}
		
		return false;
	}
	
	public int getCost() {
		return cost;
	}
	
	public String getStart() {
		return start.getLabel();
	}
	
	public String getEnd() {
		return end.getLabel();
	}

}
