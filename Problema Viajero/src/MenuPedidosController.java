import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class MenuPedidosController {
	
	@FXML
	private ChoiceBox<String> partidaChoice, finChoice;
	
	@FXML
	private TableView<Pedido> pedidosTable;
	
	@FXML
	private TableColumn<Pedido, String> colInicio, colFin;
	
	@FXML
	private Button addButton, calcularButton, borrarPedidoButton;
	
	@FXML
	private Label errorLabel;
	
	static ObservableList<Pedido> contenidoTabla = FXCollections.observableArrayList();
	ObservableList<String> puntosPedido = FXCollections.observableArrayList();
	Mapa mapa;
	
	@FXML
	public void initialize() {
		mapa = Mapa.getInstance();
		rellenarPuntosPedido();
		pedidosTable.setItems(contenidoTabla);
		colInicio.setCellValueFactory(new PropertyValueFactory<>("inicioS"));
		colFin.setCellValueFactory(new PropertyValueFactory<>("finS"));
	}
	
	private void rellenarPuntosPedido() {	
		for(Nodo nodo : mapa.nodos) {
			if(nodo.getLabel().equals("A")) {
				continue;
			}
			
			puntosPedido.add(nodo.getLabel());
		}
		
		partidaChoice.setItems(puntosPedido);
		finChoice.setItems(puntosPedido);
	}
	
	@FXML
	private void agregarPedidoATabla() {
		errorLabel.setText("");
		if(contenidoTabla.size() < 7) {
			if(partidaChoice.getSelectionModel().isEmpty() || finChoice.getSelectionModel().isEmpty()) {
				errorLabel.setText("Rellena todos los campos");
			} else {
				String inicioS = partidaChoice.getValue();
				String finS = finChoice.getValue();
				
				Pedido pedido = new Pedido(inicioS, finS);
				contenidoTabla.add(pedido);
				pedidosTable.refresh();
				
				partidaChoice.getSelectionModel().clearSelection();
				finChoice.getSelectionModel().clearSelection();
			}
		} else {
			errorLabel.setText("Maximo 7 pedidos");
		}
		
		
	}
	
	@FXML
	private void borrarPedido() {
		Pedido toRemove = pedidosTable.getSelectionModel().getSelectedItem();
		
		if(toRemove == null) {
			errorLabel.setText("Selecciona un pedido para borrar");
		} else {
			contenidoTabla.remove(toRemove);
			pedidosTable.refresh();
		}
	}
	
	@FXML
	private void calcCamino() throws IOException {
		if(contenidoTabla.isEmpty()) {
			errorLabel.setText("A�ade pedidos para continuar");
		} else {
			for(Pedido pedido : contenidoTabla) {
				mapa.generarPedido(pedido.inicioS, pedido.finS);
			}
			
			mapa.encontrarCamino("A");
			
			FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("MenuResultadosTemplate.fxml"));
			Parent root = (Parent) fxmlLoader.load();
			Scene scene = new Scene(root, 795 , 532);
			Main.window.setScene(scene);
		}
	}

}
