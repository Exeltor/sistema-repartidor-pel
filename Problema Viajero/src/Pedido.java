
public class Pedido {
	
	Nodo inicio, fin;
	String inicioS, finS;
	
	public Pedido(String inicioS, String finS) {
		this.inicioS = inicioS;
		this.finS = finS;
	}
	
	public Pedido(Nodo inicio, Nodo fin) {
		this.inicio = inicio;
		this.fin = fin;
	}

	public String getInicio() {
		return inicio.getLabel();
	}
	
	public boolean checkStart(String label) {
		if(this.inicio.getLabel().equals(label)) {
			return true;
		}
		
		return false;
	}
	
	public boolean checkEnd(String label) {
		if(this.fin.getLabel().equals(label)) {
			return true;
		}
		
		return false;
	}
	
	public String getFin() {
		return fin.getLabel();
	}

	
	public String getInicioS() {
		return inicioS;
	}
	
	public String getFinS() {
		return finS;
	}
	
	public String toString() {
		return "Trayecto: " + inicio.getLabel() + " ------> " + fin.getLabel();
	}

}
