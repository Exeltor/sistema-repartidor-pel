import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Mapa {
	
	private static Mapa mapa;
	
	public ArrayList<Nodo> nodos = new ArrayList<>();
	ArrayList<Conexion> caminos = new ArrayList<>();
	ArrayList<String> permutaciones = new ArrayList<>();
	ArrayList<String> invalido = new ArrayList<>();
	ArrayList<Pedido> pedidos = new ArrayList<>();
	Map<String, Integer> puntuaciones = new HashMap<>();
	ArrayList<Pedido> addedPedidos = new ArrayList<>();
	String permutate = "";
	String solucion;
	
	private Mapa() {
		
	}
	
	//Recogida de nodo del arraylist(tambien sirve como comprobacion de existencia)
	private Nodo getNode(String label) {
		for(Nodo nodo : mapa.nodos) {
			if(nodo.getLabel().equals(label)) {
				return nodo;
			}
		}
		
		return null;
	}
	//Recogida de conexion del arraylist(tambien sirve como comprobacion de existencia)
	private Conexion getConnection(String startLabel, String endLabel) {
		for(Conexion conexion : mapa.caminos) {
			if(conexion.checkStartNodeLabel(startLabel) && conexion.checkEndNodeLabel(endLabel)) {
				return conexion;
			}
		}
		
		return null;
	}
	
	//Comprobaci�n si el pedido ya ha sido a�adido
	private boolean orderIsAdded(Pedido pedido) {
		for(Pedido contenido : mapa.addedPedidos) {
			if(pedido.checkStart(contenido.getInicio()) && pedido.checkEnd(contenido.getFin())) {
				return true;
			}
		}
		return false;
	}
	//Metodo creacion de un pedido
	public void generarPedido(String inicio, String fin) {
		pedidos.add(new Pedido(this.getNode(inicio), this.getNode(fin)));
	}
	//Metodo creacion de un nodo. Genera las conexiones automaticamente.
	public void addNode(String label, int x, int y) {
		if(getNode(label) == null) {
			mapa.nodos.add(new Nodo(label, x, y));
			for(Nodo nodo : mapa.nodos) {
				if(label != nodo.getLabel()) {
					addConnection(label, nodo.getLabel());
				}
			}
		}
	}
	//Metodo auxiliar usado en addNode. Establece la conexion entre 2 nodos.
	private void addConnection(String startLabel, String endLabel) {
		if(getConnection(startLabel, endLabel) == null) {
			mapa.caminos.add(new Conexion(getNode(startLabel), getNode(endLabel)));
		}
	}
	/*
	 * Metodo para encontrar el mejor camino desde un punto de inicio especificado.
	 * Este metodo especificamente genera todas las permutaciones necesarias para su posterior
	 * manipulacion en los metodos auxiliares hacerLimpieza() y generarPuntuaciones().
	 */
	public void encontrarCamino(String start) {
		if(getNode(start) != null) {
			pedidosLoop:
			for(Pedido pedido : mapa.pedidos) {
				if(pedido.getInicio().equals(start) || pedido.getFin().equals(start)) {
					System.err.println("El punto inicio o partida el pedido no puede ser el numero de partida del repartidor");
					System.exit(1);
				} else {
					if(mapa.orderIsAdded(pedido)) {
						continue pedidosLoop;
					}
					
					Pedido reverse = new Pedido(getNode(pedido.getFin()), getNode(pedido.getInicio()));
					if(mapa.orderIsAdded(reverse)) {
						mapa.permutate += reverse.getFin();
					}
					
					
					if(!mapa.permutate.contains(pedido.getInicio())) {
						mapa.permutate += pedido.getInicio();
					}
					
					if(!mapa.permutate.contains(pedido.getFin())) {
						mapa.permutate += pedido.getFin();
					}
					
					mapa.addedPedidos.add(pedido);
				}
			}
		} else {
			System.err.println("El nodo no existe");
		}
		
		permutaciones(mapa.permutate, 0, mapa.permutate.length()-1);
		
		for(int i = 0; i < mapa.permutaciones.size(); i++) {
			String finalCheck = start + mapa.permutaciones.get(i) + start;	
			mapa.permutaciones.set(i, finalCheck);
		}
		
		for(Pedido pedido : mapa.pedidos) {
			System.out.println(pedido);
		}
		
		hacerLimpieza();
		generarPuntuaciones();
		
	}
	
	//Imprime las permutaciones contenidas en el ArrayList
	public void mostrarPermutaciones() {
		for(String permutacion : mapa.permutaciones) {
			System.out.println(permutacion);
		}
	}
	/**
	 * Metodo auxiliar que comprueba la validez de las permutaciones generadas y contenidas
	 * en el ArrayList permutaciones. El metodo esta compuesto por un for-loop general marcado
	 * por la etiqueta "permLoop". Este loop itera a traves de las permutaciones existentes
	 * en el ArrayList permutaciones. El siguiente for-loop itera sobre los elementos contenidos
	 * en el ArrayList de pedidos, dentro del cual se contiene un boolean "found" para marcar si
	 * se ha encontrado el orden correcto de puntos de inicio y final de cada pedido. Si todos los
	 * puntos de inicio y final estan correctamente posicionados, se pasa a la siguiente iteracion,
	 * en el caso de que no lo esten, esta permutacion se a�ade al ArrayList de las combinaciones
	 * invalidas y se salta a la siguiente permutacion para optimizar tiempo de ejecucion, ya que
	 * la permutacion ya no cumple con los criterios.
	 * 
	 * Finalmente, se borran las permutaciones incorrectas del ArrayList principal de permutaciones
	 * utilizando el ArrayList auxiliar de combinaciones invalidas.
	 */
	private void hacerLimpieza() {
		permLoop:
		for(int k = 0; k < mapa.permutaciones.size(); k++) {
			String permutacion = mapa.permutaciones.get(k);
			for(Pedido pedido : mapa.pedidos) {
				boolean found = false;
				for(int i = 0; i < permutacion.length(); i++) {
					String currNode = String.valueOf(permutacion.charAt(i));
					String start = pedido.getInicio();
					String end = pedido.getFin();
					
					if(currNode.equals(start)) {
						for(int j = i; j < permutacion.length(); j++) {
							if(String.valueOf(permutacion.charAt(j)).equals(end)) {
								found = true;
							}
						}
					}
				}
				
				if(!found) {
					mapa.invalido.add(permutacion);
					continue permLoop;
				}
			}
		}
	
		for(String invalido : mapa.invalido) {
			for(int i = 0; i < mapa.permutaciones.size(); i++) {
				if(mapa.permutaciones.get(i).equals(invalido)) {
					mapa.permutaciones.remove(i);
				}
			}
		}
		
	}
	
	/**
	 * Metodo auxiliar utilizado para calcular el coste de cada trayecto. Se utiliza un
	 * HashMap para almacenar los valores obtenidos, ya que este metodo de almacenamiento
	 * ofrece una asociacion eficiente entre objeto y valor. Este metodo itera por
	 * el ArrayList permutaciones, caracter por caracter, para asi poder leer los costes
	 * de las conexiones entre los nodos del ArrayList conexiones, y poder sumarlos para
	 * su posterior almacenamiento en el HashMap puntuaciones.
	 */
	private void generarPuntuaciones() {
		for(String permutacion : mapa.permutaciones) {
			int puntuacion = 0;
			for(int i = 0; i < permutacion.length()-1; i++) {
				puntuacion += getConnection(String.valueOf(permutacion.charAt(i)), String.valueOf(permutacion.charAt(i+1))).getCost();
			}
			
			mapa.puntuaciones.put(permutacion, puntuacion);
		}
	}
	
	//Metodo para obtener el coste minimo almacenado en el HashMap puntuaciones
	public void encontrarSolucion() {
		int min = Integer.MAX_VALUE;
		String finalPerm = "";
		
		for(String key : mapa.puntuaciones.keySet()) {
			if(mapa.puntuaciones.get(key) < min) {
				min = mapa.puntuaciones.get(key);
				finalPerm = key;
			}
		}
		
		mapa.solucion = finalPerm + ", Puntuacion: " + mapa.puntuaciones.get(finalPerm);
		//System.out.println(solucion);
	}
	
	//Metodo recursivo backtracking para encontrar todas las permutaciones de una String
	private void permutaciones(String str, int startInd, int endInd) {
		if(startInd == endInd) {
			mapa.permutaciones.add(str);
		} else {
			for(int i = startInd; i <= endInd; i++) {
				str = intercambio(str, startInd, i);
				permutaciones(str, startInd+1, endInd);
				str = intercambio(str, startInd, i);
			}
		}
	}
	
	//Metodo auxiliar de "permutaciones()" para intercambiar los sitios de los caracteres.
	private String intercambio(String str, int i, int j) {
		char temp;
		char[] charArray = str.toCharArray();
		temp = charArray[i];
		charArray[i] = charArray[j];
		charArray[j] = temp;
		return String.valueOf(charArray);
	}
	
	//Metodo para imprimir los contenidos del HashMap puntuaciones
	private void mostrarResultados() {
		for(String key : mapa.puntuaciones.keySet()) {
			System.out.println("Camino: " + key + ", Puntuacion: " + mapa.puntuaciones.get(key));
		}
	}
	
	//toString para mostrar los datos contenidos en el Mapa de forma general.
	public String toString() {
		System.out.println("Nodos");
		System.out.println("----------------------");
		for(Nodo nodo : mapa.nodos) {
			System.out.println(nodo.getLabel());
		}
		
		System.out.println("Pedidos");
		System.out.println("----------------------");
		for(Pedido pedido : mapa.pedidos) {
			System.out.println(pedido);
		}
		
		System.out.println("----------------------");
		System.out.println("Conexiones");
		for(Conexion conexion : mapa.caminos) {
			System.out.println(conexion.getStart() + " <-------> " + conexion.getEnd() + ", Coste: " + conexion.getCost());
		}
		
		System.out.println("----------------------");
		System.out.println("Permutaciones");
		mostrarResultados();
		
		System.out.println("----------------------");
		System.out.println("----------------------");
		System.out.println("Solucion");
		encontrarSolucion();
		
		System.out.println("----------------------");
		
		return "";
	}
	
	public static Mapa getInstance() {
		if(mapa == null) {
			mapa = new Mapa();
		}
		
		return mapa;
	}
	
	public static void resetInstance() {
		mapa = null;
	}

}
