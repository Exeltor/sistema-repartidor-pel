
public class Nodo {
	
	private String label;
	int x, y;
	
	public Nodo(String label, int x, int y) {
		this.label = label;
		this.x = x;
		this.y = y;
	}
	
	public String getLabel() {
		return label;
	}
	
	public void setLabel(String label) {
		this.label = label;
	}

}
